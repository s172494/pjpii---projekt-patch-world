#include <cstdlib>
#include <cstdio>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <conio.h>
#include <cstdlib>
#include "WorldMap.hpp"

#define no_entry_sea WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==3&&equipment[3].inStock==0
#define no_entry_lake WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==5
#define no_entry_mount WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==8||WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==7
#define no_entry_item WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==9
#define no_entry no_entry_sea||no_entry_lake||no_entry_mount||no_entry_item

int NightDay=1;
clock_t StartGame;
clock_t CurrentTime;
int pos[2][2];
int main_plates[4];

clock_t pause_time=0;

float hurt=0;

void setCursor(int x, int y)
{
    COORD c;
    c.X = x;
    c.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),c);
}

struct e
{
	int id;
	char name[20];
	bool inStock;
	int quantity;
	int max;
};

e equipment[15];

void equip()
{
	for(int i=1;i<=15;i++)
	{
		equipment[i-1].id=i;
	}
	char tempC[20];
	int tempI;
	int temp;
	int t;
	FILE *plik;
    plik = fopen("items.txt","r");
    for(int i=0;i<15;i++)
	{
		fscanf(plik,"%s %d %d %d",&tempC,&tempI,&temp,&t);
		for(int j=0;j<20;j++)
		{
			if(tempC[j]!=95) equipment[i].name[j]=tempC[j];
			else equipment[i].name[j]=32;
		}
		equipment[i].max=tempI;
		equipment[i].inStock=temp;
		equipment[i].quantity=t;
	}
	fclose(plik);
}

void noti_box()
{
	chColor(14);
	setCursor(45,30);
	printf("%c",201);
	for(int i=0;i<26;i++)
	{
		printf("%c",205);
	}
	printf("%c",187);
	for(int i=0;i<8;i++)
	{
		setCursor(45,i+31);
		printf("%c",186);
		setCursor(72,i+31);
		printf("%c",186);
	}
	setCursor(45,39);
	printf("%c",200);
	for(int i=0;i<26;i++)
	{
		printf("%c",205);
	}
	printf("%c",188);
	chColor(15);
}

void notification(char note[])
{
	clock_t start=clock();
	clock_t end;
	for(int j=0;j<144;j++)
	{
		setCursor(47+j%24,32+j/24);
		printf("%c",note[j]);
	}
	getch();
	end=clock();
	pause_time=pause_time+(end-start);
	for(int i=0;i<6;i++)
	{
		setCursor(47,i+32);
		for(int j=0;j<24;j++)
		{
			printf(" ");
		}
	}
}

void print_equip()
{
	for(int i=0;i<15;i++)
	{
		if(equipment[i].quantity==0) equipment[i].inStock=false;
	}
	chColor(224);
	setCursor(45,13);
	printf("           EQUIPMENT           ");
	chColor(14);
	for(int i=0;i<15;i++)
	{
		setCursor(45,i+14);
		printf("%c",186);
		setCursor(75,i+14);
		printf("%c",186);
	}
	setCursor(45,29);
	for(int i=0;i<31;i++)
	{
		printf("%c",223);
	}
	chColor(15);
	for(int i=0;i<29;i++)
	{
		for(int j=0;j<15;j++)
		{
			setCursor(46+i,14+j);
			printf(" ");
		}
	}
	int count=14;
	for(int i=0;i<15;i++)
	{
		if(equipment[i].inStock==true)
		{
			setCursor(49,count);
			printf("%s",equipment[i].name);
			setCursor(67,count);
			printf("%d/%d",equipment[i].quantity,equipment[i].max);
			count++;
		}
	}
}

struct city
{
	int x;
	int y;
	int a;
	int b;
	char name[20];
};

city cities[4];

void build_cities()
{
	char temp1[20]="Ebrosgan";
	char temp2[20]="Cilia";
	char temp3[20]="Andovine";
	for(int i=0;i<20;i++)
	{
		cities[1].name[i]=temp1[i];
		cities[2].name[i]=temp2[i];
		cities[3].name[i]=temp3[i];
	}
	int i = 4;
	int x,y,a,b;
	x=rand()%39;
	y=rand()%23;
	a=3+rand()%33;
	b=3+rand()%23;
	int c[5][5]={{0,0,9,0,0},{0,9,9,9,0},{9,11,11,10,10},{0,9,9,9,0},{0,0,9,0,0}};
	while(i!=0)
	{
		while(WorldMap[x][y].surface!=4||isDrawed(WorldMap[x][y].plate,main_plates,4)==1)
		{
			x=rand()%39;
			y=rand()%23;
		}
		main_plates[i]=WorldMap[x][y].plate;
		while(WorldMap[x][y].map[a][b]!=4)
		{
			a=3+rand()%33;
			b=3+rand()%33;
		}
		for(int m=0;m<5;m++)
		{
			for(int n=0;n<5;n++)
			{
				if(c[m][n]!=0)
				{
					WorldMap[x][y].map[a+(m-2)][b+(n-2)]=c[m][n];
				}
			}
		}
		cities[i].x=x;
		cities[i].y=y;
		cities[i].a=a;
		cities[i].b=b;
		i--;
	}
}

struct sh
{
	int id;
	char name[20];
	int price[3];
};

sh sell[6];
sh buy[4];

void in_shop_message(int type)
{
	chColor(14);
	setCursor(2,2);
	printf("%c",201);
	for(int i=0;i<38;i++)
	{
		printf("%c",205);
	}
	printf("%c",187);
	for(int i=0;i<36;i++)
	{
		setCursor(2,i+3);
		printf("%c",186);
		setCursor(41,i+3);
		printf("%c",186);
	}
	setCursor(2,38);
	printf("%c",200);
	for(int i=0;i<38;i++)
	{
		printf("%c",205);
	}
	printf("%c",188);
	chColor(15);
	char arr[32][35];
	char temp;
	FILE *f;
	if(type==0) f=fopen("mes/0.txt","r");
	else if(type==1) f=fopen("mes/1.txt","r");
	else if(type==2) f=fopen("mes/2.txt","r");
	else if(type==3) f=fopen("mes/3.txt","r");
	else if(type==4) f=fopen("mes/4.txt","r");
	else if(type==5) f=fopen("mes/5.txt","r");
	else if(type==6) f=fopen("mes/6.txt","r");
	else if(type==7) f=fopen("mes/7.txt","r");
	else if(type==8) f=fopen("mes/8.txt","r");
	else if(type==9) f=fopen("mes/9.txt","r");
	else if(type==10) f=fopen("mes/10.txt","r");
	else if(type==11) f=fopen("mes/11.txt","r");
	else if(type==12) f=fopen("mes/12.txt","r");
	else if(type==13) f=fopen("mes/13.txt","r");
	for(int i=0;i<32;i++)
	{
		for(int j=0;j<35;j++)
		{
			fscanf(f,"%c",&arr[i][j]);
		}
	}
	fclose(f);
	for(int i=0;i<32;i++)
	{
		setCursor(4,4+i);
		for(int j=0;j<35;j++)
		{
			printf("%c",arr[i][j]);
		}
	}
}

void prepare_shop()
{
	int tempArr[3];
	int tempI;
	char tempC[20];
	FILE *file;
	file=fopen("shopSell.txt","r");
	for(int i=0;i<6;i++)
	{
		fscanf(file,"%s %d %d %d %d",&tempC,&tempI,&tempArr[0],&tempArr[1],&tempArr[2]);
		sell[i].id=tempI;
		for(int j=0;j<3;j++)
		{
			sell[i].price[j]=tempArr[j];
		}
		for(int j=0;j<20;j++)
		{
			sell[i].name[j]=tempC[j];
		}
	}
	fclose(file);
	file=fopen("shopBuy.txt","r");
	for(int i=0;i<4;i++)
	{
		fscanf(file,"%s %d %d %d %d",&tempC,&tempI,&tempArr[0],&tempArr[1],&tempArr[2]);
		buy[i].id=tempI;
		for(int j=0;j<3;j++)
		{
			buy[i].price[j]=tempArr[j];
		}
		for(int j=0;j<20;j++)
		{
			buy[i].name[j]=tempC[j];
		}
	}
	fclose(file);
}

void shop(int type)
{
	prepare_shop();
	system("cls");
	noti_box();
	print_equip();
	int choice=0;
	in_shop_message(1);
	while(1)
	{
		if(kbhit())
		{
			int key = getch();
			if(key==32)
			{
				setCursor(6,10+2*choice);
				printf(" ");
				choice=(choice+1)%3;
				setCursor(6,10+2*choice);
				printf("*");
			}
			if(key==13) break;
		}
	}
	int inchoice=0;
	if(choice==0)
	{
		in_shop_message(0);
		in_shop_message(2);
		for(int k=0;k<4;k++)
		{
			setCursor(24,10+2*k);
			printf("%d",buy[k].price[type]);
		}
		while(1)
		{
			while(1)
			{
				if(kbhit())
				{
					int inkey = getch();
					if(inkey==32)
					{
						setCursor(6,10+2*inchoice);
						printf(" ");
						inchoice=(inchoice+1)%5;
						setCursor(6,10+2*inchoice);
						printf("*");
					}
					if(inkey==13) break;
				}
			}
			for(int k=0;k<4;k++)
			{
				if(inchoice==k)
				{
					if((equipment[buy[k].id].quantity+1)>equipment[buy[k].id].max)
					{
						char war[144]="No more space in equipment!";
						notification(war);
					}
					else if(equipment[14].quantity<buy[k].price[type])
					{
						char war[144]="Not enough money!";
						notification(war);
					}
					else
					{
						if (equipment[buy[k].id].quantity==0) equipment[buy[k].id].inStock=1;
						equipment[14].quantity=equipment[14].quantity-buy[k].price[type];
						equipment[buy[k].id].quantity++;
						print_equip();
					}
				}	
			}
			if(inchoice==4) break;
		}
		in_shop_message(0);
		in_shop_message(3);
		getch();
	}
	else if(choice==1)
	{
		in_shop_message(0);
		in_shop_message(4);
		for(int k=0;k<6;k++)
		{
			setCursor(24,10+2*k);
			printf("%d",sell[k].price[type]);
		}
		while(1)
		{
			while(1)
			{
				if(kbhit())
				{
					int inkey = getch();
					if(inkey==32)
					{
						setCursor(6,10+2*inchoice);
						printf(" ");
						inchoice=(inchoice+1)%7;
						setCursor(6,10+2*inchoice);
						printf("*");
					}
					if(inkey==13) break;
				}
			}
			for(int k=0;k<6;k++)
			{
				if(inchoice==k)
				{
					if(equipment[sell[k].id].quantity==0)
					{
						char war[144]="No more items in equipment!";
						notification(war);
					}
					else if(equipment[14].quantity>=999)
					{
						char war[144]="Too much money! (like there ever could be such thing)";
						notification(war);
					}
					else
					{
						if(equipment[14].quantity==0) equipment[14].inStock = 1;
						equipment[14].quantity=equipment[14].quantity+sell[k].price[type];
						if(equipment[14].quantity>999) equipment[14].quantity=999;
						equipment[sell[k].id].quantity--;
						print_equip();
					}
				}	
			}
			if(inchoice==6) break;
		}
		in_shop_message(0);
		in_shop_message(5);
		getch();
	}
	else if(choice==2)
	{
		in_shop_message(6);
		getch();
	}
}

void hospital()
{
	system("cls");
	noti_box();
	print_equip();
	in_shop_message(7);
	getch();
	int choice=0;
	while(1)
	{
		if(kbhit())
		{
			int key = getch();
			if(key==32)
			{
				setCursor(6,10+2*choice);
				printf(" ");
				choice=(choice+1)%4;
				setCursor(6,10+2*choice);
				printf("*");
			}
			if(key==13&&choice==0)
			{
				in_shop_message(0);
				in_shop_message(9);
				getch();
				in_shop_message(7);
			} 
			else if (key==13) break;
		}
	}
	if(choice==1)
	{
		int inchoice=0;
		in_shop_message(0);
		in_shop_message(8);
		while(1)
		{
			while(1)
			{
				if(kbhit())
				{
					int key = getch();
					if(key==32)
					{
						setCursor(6,15+2*inchoice);
						printf(" ");
						inchoice=(inchoice+1)%4;
						setCursor(6,15+2*inchoice);
						printf("*");
					}
					else if (key==13) break;
				}
			}
			if(inchoice==0)
			{
				if(equipment[12].quantity==0)
				{
					char war[144]="No fish in equipment!";
					notification(war);
				}
				else if(equipment[11].quantity==20)
				{
					char war[144]="There is no more space for food in equipment!";
					notification(war);
				}
				else
				{
					if (equipment[11].quantity==0) equipment[11].inStock=1;
					if (equipment[12].quantity==1) equipment[12].inStock=0;
					equipment[12].quantity--;
					equipment[11].quantity++;
					print_equip();
				}
			}
			else if(inchoice==1)
			{
				if(equipment[9].quantity<5)
				{
					char war[144]="No enough mushrooms in equipment!";
					notification(war);
				}
				else if(equipment[11].quantity==20)
				{
					char war[144]="There is no more space for food in equipment!";
					notification(war);
				}
				else
				{
					if (equipment[11].quantity==0) equipment[11].inStock=1;
					if (equipment[9].quantity==5) equipment[9].inStock=0;
					equipment[9].quantity=equipment[9].quantity-5;
					equipment[11].quantity++;
					print_equip();
				}
			}
			else if(inchoice==2)
			{
				if(equipment[10].quantity<5)
				{
					char war[144]="No enough wheat in equipment!";
					notification(war);
				}
				else if(equipment[13].quantity==10)
				{
					char war[144]="There is no more space for food in equipment!";
					notification(war);
				}
				else
				{
					if (equipment[13].quantity==0) equipment[13].inStock=1;
					if (equipment[10].quantity==5) equipment[10].inStock=0;
					equipment[10].quantity=equipment[10].quantity-5;
					equipment[13].quantity++;
					print_equip();
				}
			}
			else if(inchoice==3) break;
		}
		in_shop_message(0);
		in_shop_message(10);
		getch();
	}
	else if(choice==2)
	{
		if(NightDay==1)
		{
			in_shop_message(0);
			in_shop_message(11);
			getch();
		}
		else if(NightDay==0)
		{
			in_shop_message(0);
			in_shop_message(12);
			hurt = hurt-6;
			pause_time=pause_time+15*8*CLOCKS_PER_SEC;
			getch();
		}
	}
	else if(choice==3)
	{
		in_shop_message(13);
		getch();
	}
}

void castle()
{
	//quests
}

void oneColor(int x,int y,int type)
{
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<39;j++)
		{
			WorldMap[x][y].map[i][j]=type;
		}
	}
}

void baseMM()
{
	for(int x=0;x<39;x++)
	{
		for(int y=0;y<23;y++)
		{
			for(int i=0;i<39;i++)
			{
				for(int j=0;j<39;j++)
				{
					WorldMap[x][y].map[i][j]=-1;
				}
			}
			if(WorldMap[x][y].surface==4||WorldMap[x][y].surface==5||WorldMap[x][y].surface==6) oneColor(x,y,4);
		}
	}
}

int nearbyMM(int x, int y, int k, int l, int size, int type)
{
	int check=0;
	int temp;
	for(int i=-size;i<2*size;i++)
	{
		for(int j=-size;j<2*size;j++)
		{
			if(k+i>0&&k+i<39&&l+j>0&&l+j<39)
			{
				temp=WorldMap[x][y].map[k+i][l+j];
				if(temp==type) check++;
			}
		}
	}
	return check;
}

int countMM(int x,int y,int type)
{
	int check=0;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[x][y].map[i][j]==type) check++;
		}
	}
	return check;
}

int countWM(int type)
{
	int check=0;
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[j][i].surface==type) check++;
		}
	}
	return check;
}

void illness(int x,int y,int i,int j,int size,int type)
{
	int R;
	for(int k=0;k<size;k++)
	{
		while(WorldMap[x][y].map[i][j]==type)
		{
			R=1+rand()%4;
			if(R==1&&i>0) i--;
			else if(R==1&&i==0)
			{
				x--;
				i=38;
			}
			else if(R==2&&j>0) j--;
			else if(R==2&&j==0)
			{
				y--;
				j=38;
			}
			else if(R==3&&i<38) i++;
			else if(R==3&&i==38)
			{
				x++;
				i=0;
			}
			else if(R==4&&j<38) j++;
			else if(R==4&&j==38)
			{
				y++;
				j=0;
			}
		}
		WorldMap[x][y].map[i][j]=type;
	}
}

void materials()
{
	int p;
	int size;
	int X,Y;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<23;j++)
		{
			if(WorldMap[i][j].surface==4&&WorldMap[i][j].plate==main_plates[1])
			{
				p=rand()%10;
				if(p<1)
				{
					size=1+rand()%3;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,16);
				}
				p=rand()%10;
				if(p<7)
				{
					size=15+rand()%15;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,17);
				}
				p=rand()%10;
				if(p<2)
				{
					X=2+rand()%35;
					Y=2+rand()%35;
					if(WorldMap[i][j].map[X][Y]==4) WorldMap[i][j].map[X][Y]=13;
				}
			}
			else if(WorldMap[i][j].surface==5&&WorldMap[i][j].plate==main_plates[1])
			{
				p=rand()%10;
				if(p<6)
				{
					size=1+rand()%7;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,16);
				}
				p=rand()%10;
				if(p<2)
				{
					size=5+rand()%11;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,17);
				}
				p=rand()%10;
				if(p<2)
				{
					X=2+rand()%35;
					Y=2+rand()%35;
					if(WorldMap[i][j].map[X][Y]==4) WorldMap[i][j].map[X][Y]=13;
				}
			}
			else if(WorldMap[i][j].surface==4&&WorldMap[i][j].plate==main_plates[2])
			{
				p=rand()%10;
				if(p<5)
				{
					size=2+rand()%3;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,16);
				}
				p=rand()%10;
				if(p<2)
				{
					size=5+rand()%17;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,17);
				}
				p=rand()%10;
				if(p<1)
				{
					size=1+rand()%2;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,18);
				}
				p=rand()%10;
				if(p<3)
				{
					X=2+rand()%35;
					Y=2+rand()%35;
					if(WorldMap[i][j].map[X][Y]==4) WorldMap[i][j].map[X][Y]=13;
				}
			}
			else if(WorldMap[i][j].surface==4&&WorldMap[i][j].plate==main_plates[2])
			{
				p=rand()%10;
				if(p<9)
				{
					size=2+rand()%8;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,16);
				}
				p=rand()%10;
				if(p<5)
				{
					size=3+rand()%3;
					X=rand()%39;
					Y=rand()%39;
					illness(i,j,X,Y,size,18);
				}
				p=rand()%10;
				if(p<5)
				{
					X=2+rand()%35;
					Y=2+rand()%35;
					if(WorldMap[i][j].map[X][Y]==4) WorldMap[i][j].map[X][Y]=13;
				}
			}
			else if(WorldMap[i][j].surface==3||WorldMap[i][j].surface==3)
			{
				X=2+rand()%35;
				Y=2+rand()%35;
				if(WorldMap[i][j].map[X][Y]==3) WorldMap[i][j].map[X][Y]=14;
			}
		}
	}
}

void coastal_line()
{
	int Size = countWM(6);
	int a,b;
	a=rand()%39;
	b=rand()%23;
	while(Size>0)
	{
		while(WorldMap[a][b].surface!=6)
		{
			a=rand()%39;
			b=rand()%23;
		}
		for(int i=4;i<39;i+=14)
		{
			illness(a,b,i,0,50,4);
			illness(a,b,0,i,50,4);
			illness(a,b,i,37,50,4);
			illness(a,b,37,i,50,4);
		}
		WorldMap[a][b].surface=4;
		Size--;
	}
	for(int x=0;x<39;x++)
	{
		for(int y=0;y<23;y++)
		{
			for(int i=0;i<39;i++)
			{
				for(int j=0;j<39;j++)
				{
					if(WorldMap[x][y].map[i][j]==-1)
					{
						WorldMap[x][y].map[i][j]=3;
					}
					if(nearbyMM(x,y,i,j,2,4)>0&&WorldMap[x][y].map[i][j]==3)
					{
						WorldMap[x][y].map[i][j]=6;
					}
				}
			}
		}
	}
}

void pathMM(int ax,int ay,int bx, int by, int A, int B,int type)			//function generating random PATH between point [ax,ay] and point [bx,by] on the map in array of PLATES
{
	srand(time(NULL));
	int temp;
	int check=0;
	int x,y;
	x=ax;
	y=ay;
	WorldMap[A][B].map[ax][ay]=type;
	WorldMap[A][B].map[bx][by]=type;
	if(ax>bx)
	{
		if(ay>by)
		{
			while((x>bx)||(y>by))
			{
				if(x>bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x--;
						check++;
					}
				}
				if(y>by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y--;
					}
				}
				else check=0;
				WorldMap[A][B].map[x][y]=type;
			}
		}
		else if(ay<by)
		{
			while((x>bx)||(y<by))
			{
				if(x>bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x--;
						check++;
					}
				}
				if(y<by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y++;
					}
				}
				else check=0;
				WorldMap[A][B].map[x][y]=type;
			}
		}
	}
	else if(ax<bx)
	{
		if(ay>by)
		{
			while((x<bx)||(y>by))
			{
				if(x<bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						check++;
						x++;
					}
				}
				if(y>by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y--;
					}
				}
				else check=0;
				WorldMap[A][B].map[x][y]=type;
			}
		}
		else if(ay<by)
		{
			while((x<bx)||(y<by))
			{
				if(x<bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x++;
						check++;
					}
				}
				if(y<by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y++;
					}
				}
				else check=0;
				WorldMap[A][B].map[x][y]=type;
			}
		}
	}
}

void forestLakesMountains()
{
	int k=0;
	int a,b,c,d;
	for(int x=0;x<39;x++)
	{
		for(int y=0;y<23;y++)
		{
			if(WorldMap[x][y].surface==4)
			{
				a=rand()%39;
				b=rand()%39;
				k=rand()%100;
				if(k<70)
				{
					illness(x,y,a,b,100,2);
				}
			}
			if(WorldMap[x][y].surface==4)
			{
				a=rand()%39;
				b=rand()%39;
				k=rand()%100;
				if(k<20)
				{
					illness(x,y,a,b,50,5);
				}
			}
			if(WorldMap[x][y].surface==5)
			{
				a=rand()%39;
				b=rand()%39;
				c=rand()%39;
				d=rand()%39;
				pathMM(a,b,c,d,x,y,7);
			}
			for(int i=0;i<39;i++)
			{
				for(int j=0;j<39;j++)
				{
					if(WorldMap[x][y].map[i][j]==7)
					{
						illness(x,y,i,j,20,8);
					}
				}
			}
		}
	}
}

void generate_pos()
{
	int a,b;
	while(WorldMap[a][b].surface!=4||WorldMap[a][b].plate!=main_plates[1])
	{
		a=rand()%39;
		b=rand()%23;
	}
	oneColor(a,b,4);
	pos[0][0]=a;
	pos[0][1]=b;
	pos[1][0]=19;
	pos[1][1]=19;
	WorldMap[pos[0][0]][pos[0][1]].visible = 1;
}

void printColor(int nd,int a,int b,int i,int j)
{
	int temp=0;
	int day[19]={0,0,32,59,102,147,126,100,100,120,70,15,0,120,189,0,103,110,107};
	int night[19]={0,0,18,49,54,16,62,64,64,135,22,15,0,128,21,0,55,54,59};
	if(WorldMap[a][b].map[i][j]==4)
	{
		if(nd==1) temp=day[4];
		if (nd==0) temp=night[4];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==6)
	{
		if(nd==1) temp=day[6];
		if (nd==0) temp=night[6];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==3)
	{
		if(nd==1) temp=day[3];
		if (nd==0) temp=night[3];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==2)
	{
		if(nd==1) temp=day[2];
		if (nd==0) temp=night[2];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==5)
	{
		if(nd==1) temp=day[5];
		if (nd==0) temp=night[5];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==7||WorldMap[a][b].map[i][j]==8)
	{
		if(nd==1) temp=day[7];
		if (nd==0) temp=night[7];
		chColor(temp);
		printf("%c",177);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==9)
	{
		if(nd==1) temp=day[9];
		if (nd==0) temp=night[9];
		chColor(temp);
		printf("%c",176);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==10)
	{
		if(nd==1) temp=day[10];
		if (nd==0) temp=night[10];
		chColor(temp);
		printf("%c",186);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==11)
	{
		if(nd==1) temp=day[11];
		if (nd==0) temp=night[11];
		chColor(temp);
		printf(" ");
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==16)
	{
		if(nd==1) temp=day[16];
		if (nd==0) temp=night[16];
		chColor(temp);
		printf("%c",158);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==17)
	{
		if(nd==1) temp=day[17];
		if (nd==0) temp=night[17];
		chColor(temp);
		printf("%c",140);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==18)
	{
		if(nd==1) temp=day[18];
		if (nd==0) temp=night[18];
		chColor(temp);
		printf("%c",158);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==13)
	{
		if(nd==1) temp=day[13];
		if (nd==0) temp=night[13];
		chColor(temp);
		printf("%c",175);
		chColor(15);
	}
	else if(WorldMap[a][b].map[i][j]==14)
	{
		if(nd==1) temp=day[14];
		if (nd==0) temp=night[14];
		chColor(temp);
		printf("%c",254);
		chColor(15);
	}
	else
	{
		printf(" ");
	}
}

void printMM()
{
	setCursor(0,0);
	int a,b;
	a=pos[0][0];
	b=pos[0][1];
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(pos[1][0]==j&&pos[1][1]==i)
			{
				chColor(207);
				printf(" ");
				chColor(15);
			}
			else
			{
				printColor(NightDay,a,b,j,i);
			}
		}
		printf("\n");
	}
}

int ban()
{
	int check=0;
	if(no_entry) check++;
	if(check!=0) return 1;
	else return 0;
}

void printMapPointer(int k)
{
	int temp[2][2];
	setCursor(pos[1][0],pos[1][1]);
	printColor(NightDay,pos[0][0],pos[0][1],pos[1][0],pos[1][1]);
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<2;j++)
		{
			temp[i][j]=pos[i][j];
		}
	}
	if(k==119 && pos[1][1]>0) 
	{
		pos[1][1]--;
		setCursor(pos[1][0],pos[1][1]);
	}
	else if(k==119)
	{
		pos[0][1]--;
		if(pos[0][1]<0)
		{
			pos[0][1]=pos[0][1]+23;
		}
		pos[1][1]=38;
		if(ban()==0) printMM();

	}
	else if(k==97 && pos[1][0]>0)
	{
		pos[1][0]--;
		setCursor(pos[1][0],pos[1][1]);
	}
	else if(k==97)
	{
		pos[0][0]--;
		if(pos[0][0]<0)
		{
			pos[0][0]=pos[0][0]+39;
		}
		pos[1][0]=38;
		if(ban()==0) printMM();

	}
	else if(k==115 && pos[1][1]<38)
	{
		pos[1][1]++;
		setCursor(pos[1][0],pos[1][1]);
	}
	else if(k==115)
	{
		pos[0][1]=(pos[0][1]+1)%23;
		pos[1][1]=0;
		if(ban()==0) printMM();
	}
	else if(k==100 && pos[1][0]<38)
	{
		pos[1][0]++;
	}
	else if(k==100)
	{
		pos[0][0]=(pos[0][0]+1)%39;
		pos[1][0]=0;
		if(ban()==0) printMM();
	}
	int x,y,a,b;
	x=pos[0][0];
	y=pos[0][1];
	a=pos[1][0];
	b=pos[1][1];
	if(no_entry)
	{
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				pos[i][j]=temp[i][j];
			}
		}
		setCursor(pos[1][0],pos[1][1]);
		chColor(207);
		printf(" ");
		chColor(15);
 	}
 	else
 	{
 		setCursor(pos[1][0],pos[1][1]);
		chColor(207);
		printf(" ");
		chColor(15);
		WorldMap[pos[0][0]][pos[0][1]].visible=true;
	 }
	 int p;
	 if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==13)
	 {
	 	WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=4;
	 	p=rand()%100;
	 	if(equipment[1].inStock==true)
	 	{
	 		if(p<72)
	 		{
	 			for(int i=0;i<39;i++)
	 			{
	 				for(int j=0;j<39;j++)
	 				{
	 					if(WorldMap[pos[0][0]][pos[0][1]].map[i][j]==13)
	 					{
	 						WorldMap[pos[0][0]][pos[0][1]].map[i][j]==4;
						}
					}
				}
	 			if(equipment[5].quantity==0) equipment[5].inStock=true;
	 			if(equipment[5].max>equipment[5].quantity) equipment[5].quantity++;
	 			print_equip();
			}
			else
			{
				if(equipment[2].inStock==true) hurt++;
				else hurt = hurt + 2.5;
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]-1]=13;
			}
		}
		else
		{
			if(p<36)
	 		{
	 			if(equipment[5].quantity==0) equipment[5].inStock=true;
	 			if(equipment[5].max>equipment[5].quantity) equipment[5].quantity++;
	 			print_equip();
			}
			else
			{
				if(equipment[2].inStock==true) hurt++;
				else hurt = hurt + 2.5;
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=13;
			}
		}
	 }
	 if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==14)
	 {
	 	WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=3;
	 	p=rand()%100;
	 	if(equipment[1].inStock==true)
	 	{
	 		if(p<89)
	 		{
	 			if(equipment[12].quantity==0) equipment[12].inStock=true;
	 			if(equipment[12].max>equipment[12].quantity) equipment[12].quantity++;
	 			print_equip();
			}
			else
			{
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]-1]=14;
			}
		}
		else
		{
			if(p<56)
	 		{
	 			if(equipment[12].quantity==0) equipment[12].inStock=true;
	 			if(equipment[12].max>equipment[12].quantity) equipment[12].quantity++;
	 			print_equip();
			}
			else
			{
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]-1]=14;
			}
		}
	 }
}

void move(char key)
{
	if(key==119||key==97||key==115||key==100) 
	{
		printMapPointer(key);
	}
}

void enterCity(int type)
{
	system("cls");
	setCursor(0,0);
	int tab[79][25];
	FILE *f;
	if(NightDay==1) f = fopen("city1day.txt","r");
	else f = fopen("city1night.txt","r");
	for(int i=0;i<50;i++)
	{
		for(int j=0;j<79;j++)
		{
			fscanf(f,"%d ",&tab[j][i]);
		}
	}
	fclose(f);
	for(int i=0;i<25;i++)
	{
		for(int j=0;j<79;j++)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),tab[j][i]);
			printf(" ");
		}
		printf("\n");
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);
	int X[4]={36,60,20,36};
	int Y[4]={24,16,12,7};
	int temp=0;
	while(1)
	{
		if(kbhit())
		{
			int key;
			key=getch();
			if(key==13)
			{
				if(temp==1)
				{
					shop(type);
					enterCity(type);
				}
				else if(temp==2)
				{
					hospital();
					enterCity(type);
				}
				else if(temp==3)
				{
					//zamek
					enterCity(type);
				}
				system("cls");
				printMM();
				noti_box();
				print_equip();
				break;
			}
			if(key==32)
			{
				temp=(temp+1)%4;
			}
		}
		setCursor(X[temp],Y[temp]);
		chColor(69);
		printf(" ");
		Sleep(400);
		setCursor(X[temp],Y[temp]);
		chColor(tab[X[temp]][Y[temp]]);
		printf(" ");
		Sleep(400);
	}
}

void action()
{
	int p;
	if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==17)
	{
		if(equipment[9].quantity==equipment[9].max)
		{
			char war[144]="Inventory full!";
			notification(war);
		}
		else
		{
			WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=4;
			if(equipment[9].quantity==0) equipment[9].inStock=true;
			equipment[9].quantity++;
		}
	}
	else if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==2)
	{
		p=rand()%100;
		if(equipment[8].quantity==equipment[8].max)
		{
			char war[144]="Inventory full!";
			notification(war);
		}
		else
		{
			WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=4;
			if(equipment[8].quantity==0) equipment[8].inStock=true;
			equipment[8].quantity++;
			if(WorldMap[pos[0][0]][pos[0][1]].plate==main_plates[1])
			{
				if(p<26)
				{
					if(equipment[10].quantity==0) equipment[10].inStock=true;
					equipment[10].quantity++;
				}
			}
			else if(WorldMap[pos[0][0]][pos[0][1]].plate==main_plates[2])
			{
				if(p<72)
				{
					if(equipment[10].quantity==0) equipment[10].inStock=true;
					equipment[10].quantity++;
				}
			}
		}
	}
	else if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==16)
	{
		if(equipment[7].quantity==equipment[7].max)
		{
			char war[144]="Inventory full!";
			notification(war);
		}
		else
		{
			if(equipment[0].inStock==0)
			{
				char war1[144]="You need a pickaxe to   mine Iron Ore! Try      buying one in the shop  in town.";
				notification(war1);
			}
			else
			{
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=4;
				if(equipment[7].quantity==0) equipment[7].inStock=true;
				equipment[7].quantity++;
			}
		}
	}
	else if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==18)
	{
		if(equipment[6].quantity==equipment[6].max)
		{
			char war[144]="Inventory full!";
			notification(war);
		}
		else
		{
			if(equipment[0].inStock==0)
			{
				char war2[144]="You need a pickaxe to   mine Crystals! Try      buying one in the shop  in town.";
				notification(war2);
			}
			else
			{
				WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]=4;
				if(equipment[6].quantity==0) equipment[6].inStock=true;
				equipment[6].quantity++;
			}
		}
	}
	else if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==10)
	{
		char temp;
		char war[144] = "You are entering city.";
		notification(war);
	}
	else if(WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]==11)
	{
		for(int i=1;i<4;i++)
		{
			if(plates[pos[0][0]][pos[0][1]]==main_plates[i])
			{
				clock_t s = clock();
				enterCity(i);
				clock_t e = clock();
				pause_time = pause_time + (e-s);
			}
		}	
	}
	else
	{
		char war3[144]="The is no item to       interact with!";
		notification(war3);
	}
}

void main_MiniMap()
{
	baseMM();
	coastal_line();
	forestLakesMountains();
	build_cities();
	materials();
	equip();
	print_equip();
	generate_pos();
	printMM();
	noti_box();
}

void randMob()
{
	int X = pos[0][0];
	int Y = pos[0][1];
	int x,y;
	int check=0;
	int p;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[X][Y].map[i][j]==13)
			{
				check=1;
				x=i;
				y=j;
			}
		}
	}
	if(check==1)
	{
		p=rand()%1000;
		if(p<5)
		{
			WorldMap[X][Y].map[x][y]=4;
			setCursor(x,y);
			printColor(NightDay,X,Y,x,y);
			p=1+rand()%4;
			if(p==1&&x>0&&WorldMap[X][Y].map[x-1][y]==4)
			{
				if(WorldMap[X][Y].map[x-1][y]==4) x--;
			}
			if(p==2&&y>0)
			{
				if(WorldMap[X][Y].map[x][y-1]==4) y--;
			}
			if(p==3&&y<38&&WorldMap[X][Y].map[x][y+1]==4)
			{
				if(WorldMap[X][Y].map[x][y+1]==4) y++;
			}
			if(p==4&&x<38)
			{
				if(WorldMap[X][Y].map[x+1][y]==4) x++;
			}
			WorldMap[X][Y].map[x][y]=13;
			setCursor(x,y);
			printColor(NightDay,X,Y,x,y);
		}
	}
	check = 0;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[X][Y].map[i][j]==14)
			{
				check=1;
				x=i;
				y=j;
			}
		}
	}
	if(check==1)
	{
		p=rand()%1000;
		if(p<5)
		{
			WorldMap[X][Y].map[x][y]=3;
			setCursor(x,y);
			printColor(NightDay,X,Y,x,y);
			p=1+rand()%4;
			if(p==1&&x>0&&WorldMap[X][Y].map[x-1][y]==4)
			{
				if(WorldMap[X][Y].map[x-1][y]==3) x--;
			}
			if(p==2&&y>0)
			{
				if(WorldMap[X][Y].map[x][y-1]==3) y--;
			}
			if(p==3&&y<38&&WorldMap[X][Y].map[x][y+1]==4)
			{
				if(WorldMap[X][Y].map[x][y+1]==3) y++;
			}
			if(p==4&&x<38)
			{
				if(WorldMap[X][Y].map[x+1][y]==3) x++;
			}
			WorldMap[X][Y].map[x][y]=14;
			setCursor(x,y);
			printColor(NightDay,X,Y,x,y);
		}
	}
}

/*
FUNCTIONS SHOULD BE CALLED IN THE FOLLOWING ORDER:
	
BASE IN WM
	tectonic();
	base();
	fill_tectonic();
	baseWM();
	sea();
	baseWM();
	land();
	mountains();
	coast();
BASE IN MM
	baseMM();
	coastal_line();
	generate_pos();
	printMM();
*/
