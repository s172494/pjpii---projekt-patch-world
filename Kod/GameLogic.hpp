#include <cstdlib>
#include <cstdio>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <conio.h>
#include <cstdlib>
#include "GameOver.hpp"

#define TLO WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]

#define COND1 TLO==16&&equipment[0].inStock==true&&equipment[7].max>equipment[7].quantity
#define COND2 TLO==2&&equipment[8].max>equipment[8].quantity
#define COND3 TLO==18&&equipment[0].inStock==true&&equipment[6].max>equipment[6].quantity
#define COND4 TLO==17&&equipment[9].max>equipment[9].quantity
#define COND COND1||COND2||COND3||COND4

int timeDelay[20]={0,0,350,220,170,0,250,400,300,0,120,170,0,0,0,0,170,220,170};
int excavation[4][2]={{16,1000},{17,500},{18,1000},{2,800}};

float life = 10;
float hunger = 10;

int D,H,M = 0;

void printLH()
{
	if(life>10) life=10;
	if(hunger>10) hunger=10;
	chColor(14);
	setCursor(45,2);
	printf("%c",201);
	for(int i=0;i<18;i++)
	{
		printf("%c",205);
	}
	printf("%c",187);
	for(int i=0;i<3;i++)
	{
		setCursor(45,i+3);
		printf("%c",186);
		setCursor(64,i+3);
		printf("%c",186);
	}
	setCursor(45,6);
	printf("%c",200);
	for(int i=0;i<18;i++)
	{
		printf("%c",205);
	}
	printf("%c",188);
	setCursor(46,8);
	chColor(15);
	printf("HUNGER:");
	setCursor(57,8);
	chColor(12);
	if(hunger==0)
	{
		for(int i=0;i<10;i++)
		{
			printf("  ");
		}
	}
	else if(hunger==(int)hunger)
	{
		for(int i=0;i<hunger;i++)
		{
			printf("%c ",219);
		}
		for(int i=0;i<(10-hunger);i++)
		{
			printf("  ");
		}
	}
	else
	{
		for(int i=0;i<hunger-1;i++)
		{
			printf("%c ",219);
		}
		printf("%c",220);
		for(int i=0;i<(9.5-hunger);i++)
		{
			printf("  ");
		}
	}
	chColor(15);
	setCursor(46,10);
	printf("LIFE:");
	setCursor(57,10);
	chColor(11);
	if(life==0)
	{
		for(int i=0;i<10;i++)
		{
			printf("  ");
		}
	}
	else if(life==(int)life)
	{
		for(int i=0;i<life;i++)
		{
			printf("%c ",219);
		}
		for(int i=0;i<(10-life);i++)
		{
			printf("  ");
		}
	}
	else
	{
		for(int i=0;i<life-1;i++)
		{
			printf("%c ",219);
		}
		printf("%c",220);
		for(int i=0;i<(9.5-life);i++)
		{
			printf("  ");
		}
	}
	chColor(15);
}

int constant=0;
int check=0;
clock_t SaveTime=0;

void saveOpt()
{
	SaveTime = SaveTime + clock();
	mkdir("save");
	mkdir("save/tect");
	mkdir("save/worldmap");
	mkdir("save/minimap");
	mkdir("save/gamelogic");
	FILE *f;
	f = fopen("save/tect/integers.txt","w");
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			fprintf(f,"%d ",plates[j][i]);
		}
		fprintf(f,"\n");
	}
	fprintf(f,"\n\n");
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<300;j++)
		{
			fprintf(f,"%d ",border[i][j]);
		}
		fprintf(f,"\n");
	}
	fprintf(f,"\n\n");
	fprintf(f,"%d\n",n_border);
	fprintf(f,"%d",number);
	fclose(f);
	f = fopen("save/worldmap/integers.txt","w");
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			fprintf(f,"%d ",WorldMap[j][i].border);
			fprintf(f,"%d ",WorldMap[j][i].plate);
			fprintf(f,"%d ",WorldMap[j][i].time_zone);
			fprintf(f,"%d ",WorldMap[j][i].surface);
			fprintf(f,"%d ",WorldMap[j][i].visible);
			fprintf(f,"\n");
		}
	}
	fprintf(f,"\n\n");
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			for(int a=0;a<39;a++)
			{
				for(int b=0;b<39;b++)
				{
					fprintf(f,"%d ",WorldMap[j][i].map[a][b]);
				}
			}
			fprintf(f,"\n");
		}
	}
	fclose(f);
	f = fopen("save/minimap/integers.txt","w");
	fprintf(f,"%d\n",NightDay);
	fprintf(f,"%d\n",StartGame);
	fprintf(f,"%d\n",CurrentTime);
	fprintf(f,"%d\n",pause_time);
	fprintf(f,"\n");
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<2;j++)
		{
			fprintf(f,"%d ",pos[i][j]);
		}
		fprintf(f,"\n");
	}
	fprintf(f,"\n");
	for(int i=0;i<4;i++)
	{
		fprintf(f,"%d ",main_plates[i]);
	}
	fprintf(f,"\n\n");
	for(int i=0;i<15;i++)
	{
		fprintf(f,"%d %d\n",equipment[i].inStock,equipment[i].quantity);
	}
	fprintf(f,"\n");
	for(int i=0;i<4;i++)
	{
		fprintf(f,"%d %d %d %d\n",cities[i].x,cities[i].y,cities[i].a,cities[i].b);
	}
	fclose(f);
	f = fopen("save/minimap/float.txt","w");
	fprintf(f,"%f",hurt);
	fclose(f);
	f = fopen("save/gamelogic/float.txt","w");
	fprintf(f,"%f\n",life);
	fprintf(f,"%f\n",hunger);
	fclose(f);
	f = fopen("save/gamelogic/integer.txt","w");
	fprintf(f,"%d %d %d %d\n",D,H,M,SaveTime);
	fprintf(f,"%d %d\n",constant,check);
	fclose(f);
}

void setTime()
{
	int day=1;
	int hour=0;
	int minute=0;
	CurrentTime = clock() + SaveTime - StartGame - pause_time;
	int temp = WorldMap[pos[0][0]][pos[0][1]].time_zone;
	clock_t T = CurrentTime+7*15*CLOCKS_PER_SEC+temp*15*CLOCKS_PER_SEC;
	while(T>360*CLOCKS_PER_SEC)
	{
		T=T-360*CLOCKS_PER_SEC;
		day++;
	}
	while(T>15*CLOCKS_PER_SEC)
	{
		T=T-15*CLOCKS_PER_SEC;
		hour++;
	}
	while(T>0.25*CLOCKS_PER_SEC)
	{
		T=T-0.25*CLOCKS_PER_SEC;
		minute++;
	}
	if(check==0)
	{
		check=1;
		constant=hour+1;
	}
	if(hour==constant+1&&hunger>0)
	{
		hunger=hunger-0.5;
		printLH();
		check=0;
	}
	if(hour==constant&&hunger==0)
	{
		life=life-0.5;
		printLH();
		check=0;
	}
	setCursor(47,4);
	printf("DAY: %4d  %02d:%02d",day,hour,minute);
	D=day;
	H=hour;
	M=minute;
	if(H>=21||H<=6) NightDay=0;
	else NightDay=1;
}

void iNMTitle()
{
    char title[12][33];
    FILE *file; 
	char temp;
    file = fopen("GameMenu.txt","r");
    for(int i=0;i<12;i++)
	{
		for(int j=0;j<33;j++)
		{
			fscanf(file,"%c",&temp);
			title[i][j]=temp;
		}
	}
	fclose(file);
	chColor(10);
	for(int i=0;i<12;i++)
	{             
		for(int j=0;j<33;j++)
		{
			printf("%c",title[i][j]);
		}
	}
	chColor(15);
}

void inGameMenu()
{
	chColor(15);
	system("cls");
	chColor(10);
	setCursor(9,13);
	printf(" GO TO MINIMAP ");
	setCursor(8,15);
	printf(" GO TO  PLATEMAP ");
	setCursor(8,17);
	printf(" GO TO  WORLDMAP ");
	setCursor(9,19);
	printf(" SAVE AND EXIT ");
	setCursor(6,21);
	printf(" EXIT WITHOUT SAVING ");
	int iGMchoice=0;
	setCursor(0,1);
	iNMTitle();
	while(1)
	{
		if(kbhit())
		{
			char key;
    		key = getch();
    		if(key==32)
    		{
    			iGMchoice=(iGMchoice+1)%5;
			}
			else if(key==13)
			{
				break;
			}
		}
		if(iGMchoice==0)
		{
			chColor(160);
			setCursor(9,13);
			printf(" GO TO MINIMAP ");
		}
		else if(iGMchoice==1)
		{
			chColor(160);
			setCursor(8,15);
			printf(" GO TO  PLATEMAP ");
		}
		else if(iGMchoice==2)
		{
			chColor(160);
			setCursor(8,17);
			printf(" GO TO  WORLDMAP ");
		}
		else if(iGMchoice==3)
		{
			chColor(160);
			setCursor(9,19);
			printf(" SAVE AND EXIT ");
		}
		else if(iGMchoice==4)
		{
			chColor(160);
			setCursor(6,21);
			printf(" EXIT WITHOUT SAVING ");
		}
		Sleep(300);
		chColor(10);
		setCursor(9,13);
		printf(" GO TO MINIMAP ");
		setCursor(8,15);
		printf(" GO TO  PLATEMAP ");
		setCursor(8,17);
		printf(" GO TO  WORLDMAP ");
		setCursor(9,19);
		printf(" SAVE AND EXIT ");
		setCursor(6,21);
		printf(" EXIT WITHOUT SAVING ");
		Sleep(300);
	}
	if(iGMchoice==0)
	{
		system("cls");
	}
	else if(iGMchoice==1)
	{
		system("cls");
		print_p_makro();
		while(1)
		{
			if(kbhit()) break;
			Sleep(200);
			setCursor(2*pos[0][0],pos[0][1]);
			chColor(69);
			printf("  ");
			Sleep(200);
			setCursor(2*pos[0][0],pos[0][1]);
			chColor(color[plates[pos[0][0]][pos[0][1]]]);
			printf("  ");
		}
		system("cls");
		inGameMenu();
	}
	else if(iGMchoice==2)
	{
		system("cls");
		print_WorldMap();
		while(1)
		{
			if(kbhit()) break;
			Sleep(200);
			setCursor(2*pos[0][0],pos[0][1]);
			chColor(69);
			printf("  ");
			Sleep(200);
			setCursor(2*pos[0][0],pos[0][1]);
			chColor(colorWM[WorldMap[pos[0][0]][pos[0][1]].surface]);
			printf("  ");
		}
		system("cls");
		inGameMenu();
	}
	else if(iGMchoice==3)
	{
		saveOpt();
		exit(0);
	}
	else if(iGMchoice==4)
	{
		exit(0);
	}
	printLH();
	printMM();
	noti_box();
	print_equip();
}

void inGameLogic()
{
	chColor(15);
	while(1)
	{
		setTime();
		printLH();
		if(life==0)
		{
			goPrint();
			getch();
			exit(0);
		}
		if(kbhit())
		{
			char key;
    		key = getch();
    		if(key==27)		//saving
    		{
    			clock_t s = clock();
    			inGameMenu();
    			clock_t e = clock();
    			pause_time=pause_time+(e-s);
			}
			else if(key==119||key==97||key==115||key==100) 	//movement
			{
				int p;
				clock_t T = clock();
				int time = timeDelay[WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]]];
				move(key);
				life=life-hurt;
				hurt=0;
				while(T+time>clock())
				{
					setTime();
					p=rand()%10;
					if(p<5) randMob();
				}
				if(WorldMap[pos[0][0]][pos[0][1]].visible==0) WorldMap[pos[0][0]][pos[0][1]].visible=1;
			}
			else if(key==113)	//excavation
			{
				int a = WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]];
				if(COND)
				{
					clock_t T = clock();
					int counter=0;
					int time;
					int a = WorldMap[pos[0][0]][pos[0][1]].map[pos[1][0]][pos[1][1]];
					for(int i=0;i<4;i++)
					{
						if(a==excavation[i][0]) time = excavation[i][1];
					}
					while(T+time>clock())
					{
						setTime();
					}
				}
				action();
				print_equip();
			}
			else if(key==101)			//enter to equipment
			{
				COORD c;
				clock_t start = clock();
				clock_t end;
				int choice=0;
				int E=0;
				setCursor(47,14);
				printf("%c",158);
				while(1)
				{
					int current[15];
					int count=0;
					if(kbhit())
					{
						char K;
    					K = getch();
						for(int i=0;i<15;i++)
						{
							if(equipment[i].inStock==true)
							{
								current[count]=equipment[i].id;
								count++;
							}
						}
    					if(K==101)
    					{
    						end=clock();
    						pause_time = pause_time + (end-start);
    						for(int i=0;i<count;i++)
							{
								setCursor(47,14+i);
								printf(" ");
							}
    						break;
						}
						else if(K==32)
						{
							choice=(choice+1)%count;
						}
						else if(K==13)
						{
							if(current[choice]==12) //food rations
							{
								if(equipment[11].quantity>0)
								{
									if(hunger<10||life<10)
									{
										equipment[11].quantity--;
										if(hunger<7) hunger=hunger+3;
										else hunger=10;
										if(life<9) life++;
										else life = 10;
										print_equip();
									}
									else E=1;
								}
								else
								{
									char warn[144] = "No such item in inventory!";
									notification(warn);
								}
							}
							else if(current[choice]==11) //mushrooms
							{
								if(equipment[10].quantity>0)
								{
									if(hunger<10)
									{
										equipment[10].quantity--;
										if(hunger<9) hunger=hunger+1;
										else hunger=10;
										if(life>1) life--;
										else life = 0;
										print_equip();
									}
									else E=1;
								}
								else
								{
									char warn[144] = "No such item in inventory!";
									notification(warn);
								}
							}
							else if(current[choice]==14) //health potions
							{
								if(equipment[13].quantity>0)
								{
									if(life<10)
									{
										equipment[13].quantity--;
										if(life<7) life=life+3;
										else life = 10;
										print_equip();
									}
									else E=1;
								}
								else
								{
									char warn[144] = "No such item in inventory!";
									notification(warn);
								}
							}
							else if(current[choice]==13) //fish
							{
								if(equipment[12].quantity>0)
								{
									if(hunger<10)
									{
										equipment[12].quantity--;
										if(hunger<8) hunger=hunger+2;
										else hunger = 10;
										print_equip();
									}
									else E=1;
								}
								else
								{
									char warn[144] = "No such item in inventory!";
									notification(warn);
								}
							}
							else E=1;
							end=clock();
    						pause_time = pause_time + (end-start);
    						for(int i=0;i<count;i++)
							{
								setCursor(47,14+i);
								printf(" ");
							}
    						break;
						}
					}
					for(int i=0;i<count;i++)
					{
						setCursor(47,14+i);
						if(i==choice) printf("%c",158);
						else printf(" ");
					}
				}
				if(E==1)
				{
					char war[144]="You can't use this item now!";
					notification(war);
				}
			}
		}
		randMob();
		setTime();
		if((H>=21||H<=7)&&NightDay==1) printMM();
		if(H<=21&&H>=7&&NightDay==0) printMM();
	}
}

