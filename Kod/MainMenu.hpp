#include <cstdlib>
#include <vector>
#include <cstdio>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <dir.h> 
#include <process.h> 
#include <conio.h>
#include <cstdlib>
#include "GameLogic.hpp"

void makeTitle()			//function getting ASCII art from txt file
{
	printf("\n\n\n\n");
    char temp;
    char game_title[18][78];
    FILE *file;
    file = fopen("Game_title.txt","r");
    if ((file=fopen("Game_title.txt", "r"))==NULL)
	{
    	perror("Error");
    	exit(0);
    }
    for(int i=0;i<18;i++)
	{
		for(int j=0;j<78;j++)
		{
			fscanf(file,"%c",&temp);
			game_title[i][j]=temp;
		}
	}
	fclose(file);
	for(int i=0;i<18;i++)
	{
		for(int j=0;j<78;j++)
		{
			if(game_title[i][j]==61)
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),1);
			}
			else
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),10);
			}
			printf("%c",game_title[i][j]);
		}
		//printf("\n");
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);
}

int currentChoice = 0;			//variable keeping the choice of a player

void loadOpt()
{
	FILE *f;
	f = fopen("save/tect/integers.txt","r");
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			fscanf(f,"%d ",&plates[j][i]);
		}
	}
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<300;j++)
		{
			fscanf(f,"%d ",&border[i][j]);
		}
	}
	fscanf(f,"%d\n",&n_border);
	fscanf(f,"%d",&number);
	fclose(f);
	f = fopen("save/worldmap/integers.txt","r");
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			fscanf(f,"%d ",&WorldMap[j][i].border);
			fscanf(f,"%d ",&WorldMap[j][i].plate);
			fscanf(f,"%d ",&WorldMap[j][i].time_zone);
			fscanf(f,"%d ",&WorldMap[j][i].surface);
			fscanf(f,"%d ",&WorldMap[j][i].visible);
		}
	}
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			for(int a=0;a<39;a++)
			{
				for(int b=0;b<39;b++)
				{
					fscanf(f,"%d ",&WorldMap[j][i].map[a][b]);
				}
			}
		}
	}
	fclose(f);
	f = fopen("save/minimap/integers.txt","r");
	fscanf(f,"%d\n",&NightDay);
	fscanf(f,"%d\n",&StartGame);
	fscanf(f,"%d\n",&CurrentTime);
	fscanf(f,"%d\n",&pause_time);
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<2;j++)
		{
			fscanf(f,"%d ",&pos[i][j]);
		}
	}
	for(int i=0;i<4;i++)
	{
		fscanf(f,"%d ",&main_plates[i]);
	}
	for(int i=0;i<15;i++)
	{
		fscanf(f,"%d %d\n",&equipment[i].inStock,&equipment[i].quantity);
	}
	for(int i=0;i<4;i++)
	{
		fscanf(f,"%d %d %d %d\n",&cities[i].x,&cities[i].y,&cities[i].a,&cities[i].b);
	}
	fclose(f);
	f = fopen("save/minimap/float.txt","r");
	fscanf(f,"%f",&hurt);
	fclose(f);
	f = fopen("save/gamelogic/float.txt","r");
	fscanf(f,"%f\n",&life);
	fscanf(f,"%f\n",&hunger);
	fclose(f);
	f = fopen("save/gamelogic/integer.txt","r");
	fscanf(f,"%d %d %d %d\n",&D,&H,&M,&SaveTime);
	fscanf(f,"%d %d\n",&constant,&check);
	fclose(f);
	printMM();
	noti_box();
	print_equip();
}

void options()				//function allowing the choice
{
	equip();
	int choice=0;
	chColor(10);
	setCursor(35,24);
	printf("NEW GAME");
	setCursor(34,26);
	printf("LOAD  GAME");
	setCursor(37,28);
	printf("EXIT");
	while(1)
	{
		if(kbhit())
		{
			char key;
    		key = getch();
    		if(key==32)
    		{
    			currentChoice=(currentChoice+1)%3;
			}
			else if(key==13)
			{
				break;
			}
		}
		if(currentChoice==0)
		{
			chColor(160);
			setCursor(34,24);
			printf(" NEW GAME ");
		}
		else if(currentChoice==1)
		{
			chColor(160);
			setCursor(33,26);
			printf(" LOAD  GAME ");
		}
		else if(currentChoice==2)
		{
			chColor(160);
			setCursor(36,28);
			printf(" EXIT ");
		}
		Sleep(400);
		chColor(10);
		setCursor(34,24);
		printf(" NEW GAME ");
		setCursor(33,26);
		printf(" LOAD  GAME ");
		setCursor(36,28);
		printf(" EXIT ");
		Sleep(400);
	}
	if(currentChoice==0)
	{
		system("cls");
		StartGame=StartGame+clock();
		main_WorldMap();
		main_MiniMap();
	}
	else if(currentChoice==1)
	{
		system("cls");
		loadOpt();
	}
	else if(currentChoice==2)
	{
		exit(0);
	}
}

void main_MainMenu()
{
	MoveWindow(GetConsoleWindow(), 350, 200, 670, 700, TRUE);
	makeTitle();
	options();
	inGameLogic();
}

/*
FUNCTIONS SHOULD BE CALLED IN THE FOLLOWING ORDER:
	MoveWindow(GetConsoleWindow(), 350, 20, 2000, 700, TRUE);
	makeTitle();
	options();
*/
