#include <cstdlib>
#include <cstdio>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <conio.h>
#include <cstdlib>
#include "tect.hpp"

//Representation of numbers in WorldMap:
//	a) border: 0 - not on border; 1 - on border (of plates)
//  b) plate: -1 - background; 1-n - next plates
//  c) time zone: 1-8
//  d) climate zone: -2(cold),-1(mild cold),0(mild),1(mild hot),2(hot)
//  e) surface: -1 - background; 1 - deep sea(border); 2 - deap sea(added); 3 - sea; 4 - land; 5 - mountains; 6 - coast

struct minimap
{
	bool border;
	int map[39][39];
	int plate;
	int time_zone;
	int climat_zone;
	int surface;
	bool visible;
};

/*MINIMAP
-1- background
2 - forest
3 - sea
4 - land
5 - lakes
6 - coast
7 - mountains
8 - mountains added
9 - city
10 - city
11 - city
12 - mountains added 2
13 - wolf
14 - fish 

16 - iron
17 - wheat
18 - crystals
*/

minimap WorldMap[39][23];		//sets value of border,plate,timezone and climatezone for array WorldMap

int base_arr[23]={11,8,4,4,3,3,1,1,1,0,0,0,0,0,1,1,1,3,3,4,4,8,11};

void baseWM()
{
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<base_arr[i];j++)
		{
			WorldMap[j][i].surface=-1;
			for(int k=0;k<39;k++)
			{
				for(int l=0;l<39;l++)
				{
					WorldMap[j][i].map[k][l]=-1;
				}
			}
		}
		for(int j=(39-base_arr[i]);j<39;j++)
		{
			WorldMap[j][i].surface=-1;
			for(int k=0;k<39;k++)
			{
				for(int l=0;l<39;l++)
				{
					WorldMap[j][i].map[k][l]=-1;
				}
			}
		}
	}
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			WorldMap[j][i].border=false;
			WorldMap[j][i].visible=false;
			if(plates[j][i]==1)
			{
				WorldMap[j][i].plate=-1;
			}
			else
			{
				WorldMap[j][i].plate=plates[j][i];
			}
			WorldMap[j][i].time_zone=1+(int)(j/8);
			if(i==1||i==0||i==22||i==21) WorldMap[j][i].climat_zone=-2;
			else if(i==2||i==3||i==20||i==19) WorldMap[j][i].climat_zone=-1;
			else if((i>=5&&i<=8)||(i>=16&&i<=19)) WorldMap[j][i].climat_zone=0;
			else if(i==9||i==10||i==15||i==14) WorldMap[j][i].climat_zone=1;
			else if(i>=11&&i<=13) WorldMap[j][i].climat_zone=2;
		}
	}
	for(int i=0;i<300;i++)
	{
		WorldMap[border[0][i]][border[1][i]].border=true;
	}
}

int nearbyWM(int x,int y,int type,int size)	
{
	int check=0;
	int temp;
	for(int i=-size;i<2*size;i++)
	{
		for(int j=-size;j<2*size;j++)
		{
			if(x+i>0&&x+i<39&&y+j>0&&y+j<23)
			{
				temp=WorldMap[x+i][y+j].surface;
				if(abs(i)+abs(j)==size)
				{
					if(temp==type) check++;
				}
			}
			
		}
	}
	return check;
}

void sea()
{
	int M=790;
	int m=85;
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[j][i].border==true)
			{
				WorldMap[j][i].surface=1;
			}
		}
	}
	int j = rand()%300;
	int k;
	int a,b;
	for(int i=0;i<m;i++)
	{
		a=border[0][j];
		b=border[1][j];
		while(WorldMap[a][b].surface==-1)
		{
			j=rand()%300;
			a=border[0][j];
			b=border[1][j];
		}
		while((nearbyWM(a,b,1,2)>0&&(WorldMap[a][b].surface==1||WorldMap[a][b].surface==2))||WorldMap[a][b].surface==-1)
		{
			k=1+rand()%4;
			if(k==1&&b>0) b--;
			else if(k==2&&a>0) a--;
			else if(k==3&&a<38) a++;
			else if(k==4&&b<38) b++;
		}
		WorldMap[a][b].surface=2;
		j=rand()%300;
	}
	for(int i=0;i<M;i++)
	{
		a=border[0][j];
		b=border[1][j];
		while(WorldMap[a][b].surface==-1)
		{
			j=rand()%300;
			a=border[0][j];
			b=border[1][j];
		}
		while((nearbyWM(a,b,1,3)>0&&(WorldMap[a][b].surface==1||WorldMap[a][b].surface==2||WorldMap[a][b].surface==3))||WorldMap[a][b].surface==-1)
		{
			k=1+rand()%4;
			if(k==1&&b>0) b--;
			else if(k==2&&a>0) a--;
			else if(k==3&&a<38) a++;
			else if(k==4&&b<38) b++;
		}
		WorldMap[a][b].surface=3;
		j=rand()%300;
	}
	int base[23]={20,12,9,5,5,4,4,2,2,2,1,1,1,2,2,2,4,4,5,5,9,12,20};
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<base[i];j++)
		{
			if(WorldMap[j][i].surface!=2&&WorldMap[j][i].surface!=1) WorldMap[j][i].surface=3;
		}
		for(int j=(39-base[i]);j<39;j++)
		{
			if(WorldMap[j][i].surface!=2&&WorldMap[j][i].surface!=1) WorldMap[j][i].surface=3;
		}
	}
	int temp;
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			temp=nearbyWM(j,i,1,1)+nearbyWM(j,i,2,1)+nearbyWM(j,i,3,1);
			if(temp>4&&WorldMap[j][i].surface==0)
			{
				WorldMap[j][i].surface=3;
			}
		}
	}
}

void land()
{
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[j][i].surface==0)
			{
				WorldMap[j][i].surface=4;
			}
		}
	}
}

int mod_pos(int x,int mod)		//positive modulo
{
	int temp = x%mod;
	while(temp<0)
	{
		temp=temp+mod;
		temp=temp%mod;
	}
	return temp;
}

void mountains()
{
	int temp_border[2][300];
	int p = 1 + rand()%4;
	if(p==1)
	{
		for(int i=0;i<300;i++)
		{
			temp_border[0][i]=mod_pos(border[0][i]-5,39);
			temp_border[1][i]=mod_pos(border[1][i]-4,23);
		}
	}
	else if(p==2)
	{
		for(int i=0;i<300;i++)
		{
			temp_border[0][i]=mod_pos(border[0][i]+5,39);
			temp_border[1][i]=mod_pos(border[1][i]-4,23);
		}
	}
	else if(p==3)
	{
		for(int i=0;i<300;i++)
		{
			temp_border[0][i]=mod_pos(border[0][i]-5,39);
			temp_border[1][i]=mod_pos(border[1][i]+4,23);
		}
	}
	else if(p==4)
	{
		for(int i=0;i<300;i++)
		{
			temp_border[0][i]=mod_pos(border[0][i]+5,39);
			temp_border[1][i]=mod_pos(border[1][i]+4,23);
		}
	}
	for(int i=0;i<300;i++)
	{
		if(WorldMap[temp_border[0][i]][temp_border[1][i]].surface==4&&(nearbyWM(temp_border[0][i],temp_border[1][i],4,1)+nearbyWM(temp_border[0][i],temp_border[1][i],5,1))>3)
		{
			WorldMap[temp_border[0][i]][temp_border[1][i]].surface=5;
		}
	}
}

int colorWM [15] = {15,147,147,56,98,70,119,0,0,0,0,0,0,0,0};

void print_WorldMap()
{
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[j][i].visible==1)
			{
				if(WorldMap[j][i].surface==1||WorldMap[j][i].surface==2)
				{
					chColor(147);
					printf("%c%c",177,177);
				}
				else if(WorldMap[j][i].surface==5)
				{
					chColor(70);
					printf("%c%c",177,177);
				}
				else if(WorldMap[j][i].surface!=-1)
				{
					chColor(colorWM[WorldMap[j][i].surface]);
					printf("  ");
				}
				else
				{
					chColor(15);
					printf("  ");
				}
			}
			else 
			{
				chColor(15);
				printf("  ");
			}
		}
		printf("\n");
	}
	chColor(15);
}

int num_continents()
{
	int tempArr[number];
	int counter=0;
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(WorldMap[j][i].surface==4||WorldMap[j][i].surface==5)
			{
				if(isDrawed(WorldMap[j][i].plate,tempArr,number)==0)
				{
					tempArr[counter]=WorldMap[j][i].plate;
					counter++;
				}
			}
		}
	}
	return counter;
}

void coast()
{
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			if(nearbyWM(j,i,4,1)+nearbyWM(j,i,5,1)+nearbyWM(j,i,6,1)<4&&WorldMap[j][i].surface==4)
			{
				WorldMap[j][i].surface=6;
			}
		}
	}
}

void main_WorldMap()
{
	srand(time(NULL));
	while(num_continents()<3)
	{
		tectonic();
		base();
		fill_tectonic();
		baseWM();
		sea();
		baseWM();
		land();
		mountains();
		coast();
	}
}

/*
FUNCTIONS SHOULD BE CALLED IN THE FOLLOWING ORDER:
	srand(time(NULL));
	tectonic();
	base();
	fill_tectonic();
	baseWM();
	sea();
	baseWM();
	land();
	mountains();
	coast();
	print_WorldMap();
*/
