#include <cstdlib>
#include <cstdio>
#include <stdlib.h>
#include <ctime>
#include <Windows.h>
#include <conio.h>
#include <cstdlib>
#include "Rand.hpp"

//Numbers on the PLATES array defines:
// 0 - NULL value (shoudn't appear in final compilation)
// 1 - Background
// 2 - Border 
// 3-12 - Next plates

void chColor(int i)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),i);
}

int plates[39][23];			//array keeping the MAP of plates

int count_space(int type)		//function returning the value equal to number of how many times specific TYPE appears in array of PLATES
{
	int check=0;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<23;j++)
		{
			if(plates[i][j]==type) check++;
		}
	}
	return check;
}

void base()						//function preparing the BACKGROUND for spherical shape of world presented on the map in array of PLATES
{
	int temp_arr[23]={11,8,4,4,3,3,1,1,1,0,0,0,0,0,1,1,1,3,3,4,4,8,11};
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<temp_arr[i];j++)
		{
			plates[j][i]=1;
		}
		for(int j=(39-temp_arr[i]);j<39;j++)
		{
			plates[j][i]=1;
		}
	}
}

void path(int ax,int ay,int bx, int by)			//function generating random PATH between point [ax,ay] and point [bx,by] on the map in array of PLATES
{
	srand(time(NULL));
	int temp;
	int check=0;
	int x,y;
	x=ax;
	y=ay;
	plates[ax][ay]=2;
	plates[bx][by]=2;
	if(ax>bx)
	{
		if(ay>by)
		{
			while((x>bx)||(y>by))
			{
				if(x>bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x--;
						check++;
					}
				}
				if(y>by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y--;
					}
				}
				else check=0;
				plates[x][y]=2;
			}
		}
		else if(ay<by)
		{
			while((x>bx)||(y<by))
			{
				if(x>bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x--;
						check++;
					}
				}
				if(y<by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y++;
					}
				}
				else check=0;
				plates[x][y]=2;
			}
		}
	}
	else if(ax<bx)
	{
		if(ay>by)
		{
			while((x<bx)||(y>by))
			{
				if(x<bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						check++;
						x++;
					}
				}
				if(y>by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y--;
					}
				}
				else check=0;
				plates[x][y]=2;
			}
		}
		else if(ay<by)
		{
			while((x<bx)||(y<by))
			{
				if(x<bx)
				{
					temp=rand()%2;
					if(temp==0)
					{
						x++;
						check++;
					}
				}
				if(y<by&&check==0)
				{
					temp=rand()%2;
					if(temp==0)
					{
						y++;
					}
				}
				else check=0;
				plates[x][y]=2;
			}
		}
	}
	
}

int nearby(int x,int y,int type,int size)	//funtion returning the value of how many cells of specific TYPE is adjacent to the cell [x,y] on the map in array of PLATES
{
	int check=0;
	int temp;
	for(int i=-1;i<2;i++)
	{
		for(int j=-1;j<2;j++)
		{
			if(x+i>0&&x+i<39&&y+j>0&&y+j<23)
			{
				temp=plates[x+i][y+j];
				if(abs(i)+abs(j)==1)
				{
					if(temp==type) check++;
				}
			}
			
		}
	}
	return check;
}

void tectonic()		//function rendering semi-random (based on randomness of function path()) generating BORDERS of plates 
{
	srand(time(NULL));
	int x,y;
	x=rand()%39;
	y=rand()%39;
	while(abs(x-y)<8)
	{
		x=rand()%39;
		y=rand()%39;
	}
	path(x,0,y,23);
	int p,r;
	p=rand()%39;
	r=rand()%39;
	while(abs(p-r)<8&&abs(p-x)<8)
	{
		p=rand()%39;
		r=rand()%39;
	}
	path(p,0,r,23);
	x=5+rand()%13;
	y=5+rand()%13;
	while(abs(x-y)<5&&x+5)
	{
		x=5+rand()%13;
		y=5+rand()%13;
	}
	path(0,x,39,y);
}

void fill(int number,int x,int y)		//recurrent function fiiling the empty plate consisting of NULL space (value 0) bounded by borders, background or other plates
{
	int a,b;
	plates[x][y]=number;
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(plates[x+i-1][y+j-1]==0&&0<=y+j-1&&y+j-1<23&&0<=x+i-1&&x+i-1<39)
			{
				fill(number,x+i-1,y+j-1);
			}
		}
	}
}

int border[2][300]={0};			//array keeping cooridantes of borders
int n_border=0;				//integral variable keeping value of amount of border cells on the map

void find_border()			//function saving cooridantes of borders and amount of border cells
{
	int count = count_space(2);
	n_border = count_space(2);
	int counter=0;
	for(int i=0;i<39;i++)
	{
		for(int j=0;j<23;j++)
		{
			if(plates[i][j]==2)
			{
				border[0][counter]=i;
				border[1][counter]=j;
				counter++;
			}
		}
	}
}

int number=0;			//integral variable keepieng value of how many PLATES there is (proper value is given in fill_tectonic funtion)

void colorborder()		//funtion changing the type of border cell to value of one of the nearest plates
{
	int size = number-4;
	int arr[size];
	int r;
	for(int i=0;i<n_border;i++)
	{
		for(int color=4;color<number;color++)
		{
			r=rand()%size+4;
			while (isDrawed(r,arr,size)==1)
			{
				r=rand()%size+4;
			}
			if(nearby(border[0][i],border[1][i],r,1)>0&&plates[border[0][i]][border[1][i]]==2)
			{
				plates[border[0][i]][border[1][i]]=r;
			}
		}
	}
	while(count_space(2)!=0) colorborder();
}

void fill_tectonic()	//function filling the plates and borders with "color" (values 3-12)
{
	int i=4;
	int x,y;
	x=rand()%39;
	y=rand()%23;
	while(count_space(0)!=0)
	{
		while(plates[x][y]!=0&&count_space(0)>0)
		{
			x=rand()%39;
			y=rand()%23;
		}
		fill(i,x,y);
		i++;
	}
	number = i;
	for(int i=1;i<38;i++)
	{
		if(plates[i][0]==2&&plates[i-1][0]!=2&&plates[i+1][0]!=2&&plates[i][1]!=2) plates[i][0]=plates[i-1][0];
		if(plates[i][22]==2&&plates[i-1][22]!=2&&plates[i+1][22]!=2&&plates[i][21]!=2) plates[i][22]=plates[i-1][22];
	}
	find_border();
	colorborder();
}

int color[13]={102,0,66,204,221,255,238,202,187,170,153,51,17};  	//array with color values

void print_p_makro()		//function printing the array of PLATES in console
{
	for(int i=0;i<23;i++)
	{
		for(int j=0;j<39;j++)
		{
			chColor(color[plates[j][i]]);
			printf("  ");
		}
		printf("\n");
	}
	chColor(15);
}

void main_tectonic()
{
	srand(time(NULL));
	tectonic();
	base();
	fill_tectonic();
	print_p_makro();
}

/*
FUNCTIONS SHOULD BE CALLED IN THE FOLLOWING ORDER:
	tectonic();
	base();
	fill_tectonic();
	print_p_makro();
*/

