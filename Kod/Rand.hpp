#include <cstdlib>		//biblioteka odpowiedzielna za u�ywane polecenia m.in. exit
#include <cstdio>		//biblioteka odpowiedzielna za u�ywane polecenia m.in. printf
#include <ctime>		//biblioteka odpowiedzielna za u�ywane polecenia m.in. NULL	
#include <Windows.h>	//biblioteka odpowiedzielna za u�ywane polecenia m.in. Sleep

int isDrawed(int n,int tab[],int size)			//tworz� funkcj�, kt�ra sprawdza, czy w wybranej tablicy znajduje si� wybrana liczba
{
	if(size<=0)									//je�eli sprawdzany rozmiar (przedzia� w) tablicy (size) jest mniejszy r�wny zeru, �adna liczba si� tam nie znajduje
	{
		return 0;								//zwracam warto�� zero jako fa�sz
	}
	int i = 0;									//deklaruj� licznik i = 0
	do											//dop�ki licznik jest mniejszy od rozmiaru (sprawdzanego przedzia�u) wykonuj� poni�sz� instrukcj�
	{
		if(tab[i]==n) return 1;					//je�eli miejsce w tablicy o indeksie i jest r�wny sprawdzanej liczbie - funkcja zwraca 1 jako prawda
		i++;									//zwi�kszam licznik
	} while (i<size);
	return 0;									//je�eli sprawdzana liczba nie jest r�wna �adnej ze sprawdzanej tablicy, zwracam 0 jako fa�sz
}
